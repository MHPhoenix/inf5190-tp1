/**
  * Copyright 2017 Jacques Berger
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *     http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
**/

// Lancée lorsque le champ des articles change. Met à jour la liste des articles.
function onArticleChange() {

    var champArticle = document.getElementById("champ-artcl");
    var article = document.getElementById("champ-article").value;

    if (article === "") {

      champArticle.value = "";
      champArticle.disabled = true;

    } else {

      champArticle.disabled = false;
      var xhr = new XMLHttpRequest();

      xhr.onreadystatechange = function() {

        if (xhr.readyState === XMLHttpRequest.DONE) {

          if (xhr.status === 200) {

            champArticle.innerHTML = xhr.responseText;
            champArticle.value = "";

          } else {
            console.log('Erreur avec le serveur');
          }

        }

      };
  
      xhr.open("GET", "/article/"+article, true);
      xhr.send();

    }

    var champArt = document.getElementById("champ-art");
    champArt.value = "";
    champArt.disabled = true;

  }


  function sendArticle() {
    var article = document.getElementById("nouvel_article").value;
    if (article !== "") {
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
          if (xhr.status === 201) {
            alert('Article ajouté à la liste');
          } else {
            console.log('Erreur avec le serveur');
          }
        }
      };
  
      xhr.open("POST", "/templates/articles/", true);
      xhr.setRequestHeader("Content-Type", "application/json");
      //xhr.send(JSON.stringify({nom:pays}));
    }
  }