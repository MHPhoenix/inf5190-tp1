import datetime
from flask import (abort, g)
import sqlite3


class Article():

    """
        Les methodes qui suivent s'inspirent des exemples
        de classe du professeur Jacques Berger
    """

    
    def __init__(self):
        self.connection = None


    def get_connection(self):

        if self.connection is None:
            self.connection = sqlite3.connect('database/db.db')
        return self.connection


    def disconnect(self):

        if self.connection is not None:
            self.connection.close()


    #Fonction pour la page d'accueil(home), presente les 5 derniers articles publies
    def get_all_article_home(self):

        cursor = self.get_connection().cursor()
        cursor.execute("SELECT * FROM article LIMIT 5")
        #WHERE (date_publication = (SELECT MIN(date_publication))
        arti = cursor.fetchall()
        return [(un_article[0], un_article[1], un_article[2], un_article[3], un_article[4]) 
        for un_article in arti]


    #Fonction pour afficher toutes les information d'un article en particulier
    #Renvoie un code 404 si l'article n'existe pas
    def get_article(self, article_id):

        cursor0 = self.get_connection().cursor()
        cursor0.execute("SELECT article_id FROM article WHERE article_id = ?",
                       (article_id,))

        if len( cursor0.fetchall() ) == 0:
            return abort (404)
        else:
            cursor = self.get_connection().cursor()
            cursor.execute("SELECT * from article WHERE article_id = ?",
                       (article_id,))
            l_article = cursor.fetchall()

            return [(artcl[0], artcl[1], artcl[2], artcl[3], artcl[4]) for artcl in l_article]


    #Fonction pour la page admin qui presente tous les articles
    def get_all_article(self):

        cursor = self.get_connection().cursor()
        cursor.execute("SELECT titre, date_publication FROM article")
        arti = cursor.fetchall()
        return [(un_article[0], un_article[1]) for un_article in arti]


    #Cree un nouvel article
    def create_article(self, titre, article_id, auteur, date_publication, paragraphe):

        cursor = self.get_connection().cursor()
        conn = self.get_connection()
        #? : Pour securiser les donnees de la base
        cursor.execute(("INSERT into article(titre, article_id, auteur, date_publication, paragraphe)"
                            " VALUES(?, ?, ?, ?, ?)"), (titre, article_id, auteur, date_publication, paragraphe))
        conn.commit()


    #Fonction pour que l'admin puisse modifier un article
    def update_article(self, id_a, title, par):

        cursor = self.get_connection().cursor()
        conn = self.get_connection()

        if title is not None:
            cursor.execute("UPDATE article SET titre = ? WHERE article_id = ?", (title, id_a))
        if par is not None:
            cursor.execute("UPDATE article SET paragraphe = ? WHERE article_id  = ?", (par, id_a))
        
        #Mise a jour de la table
        conn.commit()


    def recherche(self, texte):

        cursor = self.get_connection().cursor()
        cursor.execute("""SELECT titre, article_id, date_publication 
                        FROM article WHERE titre LIKE '%?%'
                        OR paragraphe LIKE '%?%'"""), (texte, texte)
        rech = cursor.fetchall()

        if rech is None:
            return None
        else:
            return [(cherch[0], cherch[1], cherch[2]) for cherch in rech]