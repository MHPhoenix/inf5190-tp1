from flask import (Flask, render_template, g, abort)
from flask import (request, redirect, make_response)
from data import Article

#mon application web (app)
app = Flask(__name__)

"""
    Les 2 premieres methodes qui suivent viennent des exemples
        de classe du professeur Jacques Berger
"""

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        g._database = Article()
    return g._database


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.disconnect()


#compte : pour savoir le nombre d'articles qu'on a
#singulier : l'article au singulier
#singulier : les articles au pluriel
@app.context_processor
def utility_processor():
    def pluriel(compte, singulier, plu):

        #On verifie que le compte est bien un entier
        if not isinstance(compte, int):
            #On leve une exception si ce n'est pas le cas
            raise ValueError('{} doit etre un entier'.format(compte))

        #si le pluriel n'a pas ete passe, on met s au singulier de l'article
        if plu is None:
            plu = singulier + 's'

        if compte == 0 or compte == 1:
            chaine = singulier
        else:
            chaine = plu

        return '{} {}'.format(compte, chaine)

    return dict(pluriel=pluriel)


#page d'acceuil
@app.route('/', methods=['GET'])
def index():
    les_articles = get_db().get_all_article_home()
    return render_template('pages/index.html', les_articles=les_articles)


#page pour retrouver les articles dans la barre de recherche
@app.route('/resultat/', methods=['POST'])
def recherche_article():
    texte = request.form['article']
    cherch_art = get_db().recherche(texte)
    return render_template('articles/resultat.html', cherch_art=cherch_art)


#page pour chaque article individuellement
@app.route('/article/<string:article_id>/', methods=['GET'])
def article(article_id):
    les_art = get_db().get_article(article_id)
    return render_template('articles/article.html', les_art=les_art)


#Affiche une liste de tous les articles
@app.route('/admin/', methods=['GET'])
def administrateur():
    arti = get_db().get_all_article()
    return render_template('articles/articleAdmin.html', arti=arti)


#page qui affiche le formulaire que l'article a modifier
@app.route('/admin/form/', methods=['GET'])
def formulaire_modif():
    return render_template('articles/formModif.html')


#page qui permet a l'administrateur de modifier un article
@app.route('/admin/modifier/', methods=['POST'])
def modifieur():
    tit = request.form['titre']
    id_a = request.form['article_id']
    para = request.form['paragraphe']
    art_modif = get_db().update_article(id_a, tit, para)
    return render_template('articles/modifier.html', art_modif=art_modif)


#page qui permet a l'administrateur de creer un article
@app.route('/admin/nouvel_article/', methods=['POST'])
def nouvel_art():
    return redirect('/admin-nouveau/')


#page qui affiche le formulaire
@app.route('/admin-nouveau/', methods=['GET'])
def administrateur_nouv():
    return render_template('articles/formulaire.html')


#page qui affiche le formulaire
@app.route('/admin-nouveau/resultat', methods=['POST'])
def resultat_formulaire():

    #request va permettre de recuperer les valeurs du formulaire
    tit = request.form['titre']
    id_article = request.form['article_id']
    aut = request.form['auteur']
    dat_p = request.form['date_publication']
    para = request.form['message']

    if len(tit) == 0:
        return render_template('articles/formulaire.html', erreur1="Le titre est obligatoire")
    elif len(id_article) == 0:
        return render_template('articles/formulaire.html', erreur2="L'identifiant est obligatoire")
    elif len(aut) == 0:
        return render_template('articles/formulaire.html', erreur3="Le nom de l'auteur est obligatoire")
    elif len(dat_p) == 0:
        return render_template('articles/formulaire.html', erreur4="La date est obligatoire")
    elif len(para) == 0:
        return render_template('articles/formulaire.html', erreur5="Le contenu est obligatoire")
    else:
       
        data_du_formulaire = get_db().create_article(tit, id_article, aut, dat_p, para)
        return render_template('pages/envoyer.html', data_du_formulaire=data_du_formulaire)


#Si la page demande n'existe pas
@app.errorhandler(404)
def error404(error):
    return render_template('erreurs/404.html'), 404


#Demarrage de l'application (si c'est un fichier est execute directement)
if __name__ == '__main__':
    app.run(debug=True, port=5000)

